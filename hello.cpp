#include <iostream>
#include <cmath>
#include <ctime>
#include <cstdlib>
#include <iomanip>
#include <cstring>
#include <iostream>
#include <string>
using namespace std;
using std::setw;

void math();
void random2();
void array2();
void string2();
void string3();
void reference2();
void time2();
void time3();
void booksFunc();
void printBook( struct Books *book );

int main() {
    cout << "Hello World\n";

    cout << "Size of char : " << sizeof(char) << endl;
    cout << "Size of int : " << sizeof(int) << endl;
    cout << "Size of short int : " << sizeof(short int) << endl;
    cout << "Size of long int : " << sizeof(long int) << endl;
    cout << "Size of float : " << sizeof(float) << endl;
    cout << "Size of double : " << sizeof(double) << endl;
    cout << "Size of wchar_t : " << sizeof(wchar_t) << endl;

    //math();
    //random2();
    //array2();
    //string2();
    //string3();
    //reference2();
    time2();
    time3();
    booksFunc();

    return 0;
}

void math() {
    // number definition:
    short  s = 10;
    int i = -1000;
    long   l = 100000;
    float  f = 230.47;
    double d = 200.374;

    // mathematical operations;
    cout << "sin(d) :" << sin(d) << endl;
    cout << "abs(i)  :" << abs(i) << endl;
    cout << "floor(d) :" << floor(d) << endl;
    cout << "sqrt(f) :" << sqrt(f) << endl;
    cout << "pow( d, 2) :" << pow(d, 2) << endl;
}


void random2() {
    int i,j;

    // set the seed
    srand( (unsigned)time( NULL ) );

    /* generate 10  random numbers. */
    for( i = 0; i < 10; i++ )
    {
        // generate actual random number
        j= rand();
        cout <<" Random Number : " << j << endl;
    }
}

void array2 () {
    int n[ 10 ]; // n is an array of 10 integers

    // initialize elements of array n to 0  
    for ( int i = 0; i < 10; i++ )
    {
        n[ i ] = i + 100; // set element at location i to i + 100
    }
    cout << "Element" << setw( 13 ) << "Value" << endl;

    // output each array element's value  
    for ( int j = 0; j < 10; j++ )
    {
        cout << setw( 7 )<< j << setw( 13 ) << n[ j ] << endl;
    }

}

void string2 ()
{
    char str1[10] = "Hello";
    char str2[10] = "World";
    char str3[10];
    int  len ;

    // copy str1 into str3
    strcpy( str3, str1);
    cout << "strcpy( str3, str1) : " << str3 << endl;

    // concatenates str1 and str2
    strcat( str1, str2);
    cout << "strcat( str1, str2): " << str1 << endl;

    // total lenghth of str1 after concatenation
    len = strlen(str1);
    cout << "strlen(str1) : " << len << endl;
}

void string3 ()
{
    string str1 = "Hello";
    string str2 = "World";
    string str3;
    int  len ;

    // copy str1 into str3
    str3 = str1;
    cout << "str3 : " << str3 << endl;

    // concatenates str1 and str2
    str3 = str1 + str2;
    cout << "str1 + str2 : " << str3 << endl;

    // total lenghth of str3 after concatenation
    len = str3.size();
    cout << "str3.size() :  " << len << endl;
}

void reference2 () {
    // declare simple variables
    int i;
    double d;

    // declare reference variables
    int& r = i;
    double& s = d;

    i = 5;
    cout << "Value of i : " << i << endl;
    cout << "Value of i reference : " << r  << endl;

    d = 11.7;
    cout << "Value of d : " << d << endl;
    cout << "Value of d reference : " << s  << endl;
}

void time2 () {
    // current date/time based on current system
    time_t now = time(0);

    // convert now to string form
    char* dt = ctime(&now);

    cout << "The local date and time is: " << dt << endl;

    // convert now to tm struct for UTC
    tm *gmtm = gmtime(&now);
    dt = asctime(gmtm);
    cout << "The UTC date and time is:"<< dt << endl;
}

void time3 ( ) {
    // current date/time based on current system
    time_t now = time(0);

    cout << "Number of sec since January 1,1970:" << now << endl;

    tm *ltm = localtime(&now);

    // print various components of tm structure.
    cout << "Year: "<< 1900 + ltm->tm_year << endl;
    cout << "Month: "<< 1 + ltm->tm_mon<< endl;
    cout << "Day: "<<  ltm->tm_mday << endl;
    cout << "Time: "<< 1 + ltm->tm_hour << ":";
    cout << 1 + ltm->tm_min << ":";
    cout << 1 + ltm->tm_sec << endl;
}

struct Books
{
    char  title[50];
    char  author[50];
    char  subject[100];
    int   book_id;
};

void booksFunc( )
{
    struct Books Book1;// Declare Book1 of type Book
    struct Books Book2;// Declare Book2 of type Book

    // Book 1 specification
    strcpy( Book1.title, "Learn C++ Programming");
    strcpy( Book1.author, "Chand Miyan");
    strcpy( Book1.subject, "C++ Programming");
    Book1.book_id = 6495407;

    // Book 2 specification
    strcpy( Book2.title, "Telecom Billing");
    strcpy( Book2.author, "Yakit Singha");
    strcpy( Book2.subject, "Telecom");
    Book2.book_id = 6495700;

    // Print Book1 info, passing address of structure
    printBook( &Book1 );

    // Print Book1 info, passing address of structure
    printBook( &Book2 );

}

// This function accept pointer to structure as parameter.
void printBook( struct Books *book )
{
    cout << "Book title : " << book->title <<endl;
    cout << "Book author : " << book->author <<endl;
    cout << "Book subject : " << book->subject <<endl;
    cout << "Book id : " << book->book_id <<endl;
}
